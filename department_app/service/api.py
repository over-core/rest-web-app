"""
This module contains API blueprint, classes with corresponding methods for
CRUD operations on data models and static methods used by views for rendering
certain data.
In some CRUD functions flask_restful parser is called, it fetches data from
request and validates it against defined restrictions, if data is invalid -
parser service will throw corresponding error code and message in response.
Return from functions depends on Accept header in request, and returns objects
for text/html and json for application/json.
API endpoints are defined in the end of the module.

    * ListEntities - base class for querying all objects from model in a list
    * BaseEntity - base class for querying single object from model
    * ListDepartments(ListEntities) - getting all departments, creating new one
    * ListEmployees(ListEntities) - getting all employees, creating new one
    * SelectDepartment(BaseEntity) - get, put, delete methods for single dept
    * SelectEmployee(BaseEntity) - get, put, delete methods for single employee
"""
from datetime import datetime as dt

from flask import Blueprint, abort, request

from flask_restful import Resource, Api, reqparse, fields, marshal

from department_app.models import db
from department_app.models.tables import Department, Employee


crud_bp = Blueprint("crud_bp", __name__)
api = Api(crud_bp)


class ListEntities(Resource):

    """
    Base class for list of objects from table. Subclass it and define model.
    Introduce parser settings in __init__ of subclass.
    """

    model: object
    parser = reqparse.RequestParser()
    resource_fields = {}

    def _header_check(self, db_object):
        """
        Will define which data format to return upon checking headers.
        For text/html will return object.
        For application/json will format data with marshal() and defined
        resource fields.
        """
        accept = request.headers.get("Accept")
        if accept and "text/html" in accept:
            return db_object
        return marshal(db_object, self.resource_fields)

    # TODO Refactor: there should be a better way
    def _validate_args(self, args):
        """
        Validates parsed args on edge cases and invalid input for both department
        and employee model.
        """

        if hasattr(args, "salary"):
            if args.salary is not None and args.salary <= 0:
                return abort(400, "salary should be higher than 0")

        for val in args.values():
            if val == "":
                return abort(400, "non-empty string required")

        if not any(args.values()):
            return abort(400, "no arguments provided")

        if hasattr(args, "birth_date"):
            if args.birth_date is not None:
                try:
                    dt.strptime(args.birth_date, "%Y-%m-%d")
                except ValueError:
                    return abort(400, "date is in wrong format or out of range")

        if hasattr(args, "dept_id"):
            if args.dept_id is not None:
                Department.query.get_or_404(
                    args.dept_id,
                    description=f"department not found - id({args.dept_id})",
                )
        row_exists = self.model.query.filter_by(**args).first()
        if row_exists:
            return abort(400, "such row already exists")
        return None

    def get(self):
        """
        Returns all entities of queried model.
        """
        all_rows = self.model.query.all()
        return self._header_check(all_rows)

    def post(self):
        """
        Creates a new entity of given model.
        """
        args = self.parser.parse_args()
        self._validate_args(args)
        new_row = self.model(**args)
        db.session.add(new_row)
        db.session.commit()
        return self._header_check(new_row), 201


class BaseEntity(Resource):

    """
    Base class for single object from table. Subclass it and define model.
    Introduce parser settings in __init__ of subclass.
    """

    model: object
    parser = reqparse.RequestParser()
    resource_fields = {}

    def _header_check(self, db_object):
        """
        Will define which data format to return upon checking headers.
        For text/html will return object.
        For application/json will format data with marshal() and defined
        resource fields.
        """
        accept = request.headers.get("Accept")
        if accept and "text/html" in accept:
            return db_object
        return marshal(db_object, self.resource_fields)

    # TODO Refactor: there should be a better way
    # * get rid of copy-paste
    def _validate_args(self, args):
        """
        Validates parsed args on edge cases and invalid input for both department
        and employee model.
        """

        if hasattr(args, "salary"):
            if args.salary is not None and args.salary <= 0:
                return abort(400, "salary should be higher than 0")

        for val in args.values():
            if val == "":
                return abort(400, "non-empty string required")

        if not any(args.values()):
            return abort(400, "no arguments provided")

        if hasattr(args, "birth_date"):
            if args.birth_date is not None:
                try:
                    dt.strptime(args.birth_date, "%Y-%m-%d")
                except ValueError:
                    return abort(400, "date is in wrong format or out of range")

        if hasattr(args, "dept_id"):
            if args.dept_id is not None:
                Department.query.get_or_404(
                    args.dept_id,
                    description=f"department not found - id({args.dept_id})",
                )
        row_exists = self.model.query.filter_by(**args).first()
        if row_exists:
            return abort(400, "such row already exists")
        return None

    def get(self, id_):
        """
        Returns a single entity with provided ID from given model.
        """
        row = self.model.query.get_or_404(id_)
        return self._header_check(row)

    def put(self, id_):
        """
        Changes data of entity with provided ID. Will parse passed data from
        settings defined in __init__.
        """
        row = self.model.query.get_or_404(id_)
        args = self.parser.parse_args()
        self._validate_args(args)
        for key, val in args.items():
            if val is not None:
                setattr(row, key, val)
        db.session.commit()
        return self._header_check(row), 204

    def delete(self, id_):
        """
        Deletes data of entity with provided ID.
        """
        row = self.model.query.get_or_404(id_)
        db.session.delete(row)
        db.session.commit()
        return self._header_check(row), 204


class ListDepartments(ListEntities):

    """
    Subclass for department objects.
    """

    model = Department

    resource_fields = {
        "id": fields.Integer,
        "name": fields.String,
    }

    def __init__(self):
        self.init_parser()

    def init_parser(self):
        """
        Parser init and provided arguments with restrictions for accepting data.
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument(
            "name",
            type=str,
            required=True,
            help="string is required",
            location=["form", "json"],
        )

    @staticmethod
    def get_dept_staff_avg_salary(dept_id):
        """
        Function is used in rendering department table in browser web application.
        """
        employees = Department.query.get(dept_id).employees
        staff = len(employees)
        if staff:
            total_salary = sum(person.salary for person in employees)
            avg_salary = total_salary / staff
        else:
            avg_salary = 0
        return {"staff": staff, "avg_salary": avg_salary}


class ListEmployees(ListEntities):

    """
    Subclass for employee list of objects
    """

    model = Employee
    resource_fields = {
        "id": fields.Integer,
        "first_name": fields.String,
        "second_name": fields.String,
        "birth_date": fields.DateTime(dt_format="iso8601"),
        "salary": fields.Integer,
        "dept_id": fields.Integer,
    }

    def __init__(self):
        self.init_parser()

    def init_parser(self):
        """
        Parser init and provided arguments with restrictions for accepting data.
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument(
            "first_name",
            type=str,
            required=True,
            help="non-empty string required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "second_name",
            type=str,
            required=True,
            help="non-empty string required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "birth_date",
            type=str,
            required=True,
            help="birth date in 'YYYY-MM-DD' format required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "salary",
            type=int,
            required=True,
            help="integer is required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "dept_id",
            type=int,
            required=True,
            help="integer is required",
            location=["form", "json"],
        )

    @staticmethod
    def filter_fields(string, dept_id):
        """
        Function is used in rendering employee table in browser web application
        to provide results on filtering.
        """

        if dept_id != "no_dept":
            result = Employee.query.filter(Employee.dept_id == dept_id)
        else:
            result = Employee.query

        if string:
            try:
                birthday = string.split("/")
                birthday = [dt.strptime(date, "%Y-%m-%d") for date in birthday]
            except ValueError:
                return abort(400, "date is in wrong format or out of range")
            if len(birthday) == 2:
                start = min(birthday)
                end = max(birthday)
                result = result.filter(Employee.birth_date.between(start, end))
            elif len(birthday) == 1:
                date = birthday[0]
                result = result.filter(Employee.birth_date == date)
            else:
                return abort(400, "date is in wrong format or out of range")

        return result.all()


class SelectDepartment(BaseEntity):
    """
    Sublcass for single entity from department table.
    """

    model = Department
    resource_fields = {
        "id": fields.Integer,
        "name": fields.String,
    }

    def __init__(self):
        self.init_parser()

    def init_parser(self):
        """
        Parser init and provided arguments with restrictions for accepting data.
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument(
            "name",
            type=str,
            required=True,
            help="string is required",
            location=["form", "json"],
        )

    def delete(self, id_):
        """
        Override base class method to validate the table has no relations before
        deleting and provide proper response.
        """
        row = Department.query.get_or_404(id_)
        if row.employees:
            return abort(400, "unable to delete department with employees")
        db.session.delete(row)
        db.session.commit()
        return self._header_check(row), 204


class SelectEmployee(BaseEntity):
    """
    Sublcass for single entity from employee table.
    """

    model = Employee
    resource_fields = {
        "id": fields.Integer,
        "first_name": fields.String,
        "second_name": fields.String,
        "birth_date": fields.DateTime(dt_format="iso8601"),
        "salary": fields.Integer,
        "dept_id": fields.Integer,
    }

    def __init__(self):
        self.init_parser()
        super().__init__()

    def init_parser(self):
        """
        Parser init and provided arguments with restrictions for accepting data.
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument(
            "first_name",
            type=str,
            required=False,
            help="non-empty string required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "second_name",
            type=str,
            required=False,
            help="non-empty string required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "birth_date",
            type=str,
            required=False,
            help="birth date in 'YYYY-MM-DD' format required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "salary",
            type=int,
            required=False,
            help="integer is required",
            location=["form", "json"],
        )
        self.parser.add_argument(
            "dept_id",
            type=int,
            required=False,
            help="integer is required",
            location=["form", "json"],
        )


# Creating API endpoints
api.add_resource(ListDepartments, "/api/departments")
api.add_resource(SelectDepartment, "/api/departments/<int:id_>")
api.add_resource(ListEmployees, "/api/employees")
api.add_resource(SelectEmployee, "/api/employees/<int:id_>")
