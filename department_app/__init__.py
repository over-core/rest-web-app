"""This module assembles web application with API web service from blueprints
and registers components in flask application context.

    * create_app - creates web application to be accessed from browser
    * create_api - creates API accepting HTTP requests from cli
"""

from os import path
import logging
from datetime import datetime as dt

from flask import Blueprint, Flask
from flask_bootstrap import Bootstrap

from department_app.models import db, migrate
from department_app.models.tables import Department, Employee

from department_app.service.api import crud_bp
from department_app.views.routes import view_bp

DIRNAME = path.dirname(__file__)
DIRNAME = path.join(DIRNAME, "migrations")


def create_app(config_obj):
    """Creates web application to be accessed from browser

    Returns:
        flask app with all registered blueprints
    """
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_obj)

    db.init_app(app)
    Bootstrap(app)

    # Setting up logging for debug mode
    if app.debug:
        logger = logging.getLogger()
        handler = logging.FileHandler(
            "webapp_{:%Y-%m-%d_%H-%M-%S}.log".format(dt.now())
        )
        stream = logging.StreamHandler()
        logger.addHandler(handler)
        logger.addHandler(stream)
        logger.setLevel(logging.DEBUG)

    with app.app_context():
        app.register_blueprint(view_bp)

        return app


def create_api(config_obj):
    """Creates api webservice to be accessed from cli and accepting HTTP requests

    Returns:
        flask app with all registered blueprints
    """
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_obj)

    db.init_app(app)

    if app.debug:
        logger = logging.getLogger()
        handler = logging.FileHandler(
            "webservice_{:%Y-%m-%d_%H-%M-%S}.log".format(dt.now())
        )
        stream = logging.StreamHandler()
        logger.addHandler(handler)
        logger.addHandler(stream)
        logger.setLevel(logging.DEBUG)

    with app.app_context():
        app.register_blueprint(crud_bp)

        return app


def migration(config_obj):
    """
    Used to migrate data models upon model change.
    """
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_obj)
    db.init_app(app)
    migrate.init_app(app, db, directory=DIRNAME)
    return app
