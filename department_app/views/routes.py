"""Module contains view blueprint and endpoints for browser web application.
"""
from flask import Blueprint, request, render_template, redirect, url_for

from department_app.service import api
from department_app.views.forms import EmployeeForm, DepartmentForm

view_bp = Blueprint("view_bp", __name__)


@view_bp.route("/")
@view_bp.route("/departments", methods=["GET", "POST"])
def departments():
    """
    Gets info on departments from API and renders table.
    """
    all_depts = api.ListDepartments().get()
    get_dept_info = api.ListDepartments().get_dept_staff_avg_salary
    return render_template(
        "departments.html",
        get_dept_info=get_dept_info,
        all_depts=all_depts,
    )


@view_bp.route("/add_dept/", methods=["GET", "POST"])
def add_dept():
    """
    A form for new department creation, will validate input before calling API.
    On success will render department list.
    """
    action = url_for("view_bp.add_dept")
    title = "New department"
    form = DepartmentForm()
    if form.validate_on_submit():
        api.ListDepartments().post()
        return redirect(url_for("view_bp.departments"))
    return render_template("form.html", form=form, action=action, title=title)


@view_bp.route("/edit_dept/<int:id_>", methods=["GET", "POST"])
def edit_dept(id_):
    """
    A form for existing department editing, will autofill and validate input
    before calling API. On success will render department list.
    """
    action = url_for("view_bp.edit_dept", id_=id_)
    title = "Edit department data"
    data = api.SelectDepartment().get(id_)
    form = DepartmentForm(obj=data)
    if form.validate_on_submit():
        api.SelectDepartment().put(id_)
        return redirect(url_for("view_bp.departments"))
    return render_template("form.html", form=form, action=action, title=title)


@view_bp.route("/delete_dept/<int:id_>", methods=["POST"])
def delete_dept(id_):
    """
    A delete command for department. Will not allow to delete department with
    employees.
    """
    api.SelectDepartment().delete(id_)
    return redirect(url_for("view_bp.departments"))


@view_bp.route("/employees", methods=["GET", "POST"])
def employees():
    """
    Gets info on employees from API and renders table.
    """
    form = EmployeeForm()
    dept_id = request.form.get("dept_id")
    birthday = request.form.get("birthday")
    if request.method == "POST" and (birthday or dept_id):
        all_staff = api.ListEmployees().filter_fields(birthday, dept_id)
    else:
        all_staff = api.ListEmployees().get()
    return render_template("employees.html", all_staff=all_staff, form=form)


@view_bp.route("/add_employee/", methods=["GET", "POST"])
def add_employee():
    """
    A form for new employee creation, will validate input before calling API.
    On success will render employee list.
    """
    action = url_for("view_bp.add_employee")
    title = "New employee"
    form = EmployeeForm()
    if form.validate_on_submit():
        api.ListEmployees().post()
        return redirect(url_for("view_bp.employees"))
    return render_template("form.html", form=form, action=action, title=title)


@view_bp.route("/edit_employee/<int:id_>", methods=["GET", "POST"])
def edit_employee(id_):
    """
    A form for existing employe editing, will autofill and validate input
    before transfering calling API. On success will render department list.
    """
    action = url_for("view_bp.edit_employee", id_=id_)
    title = "Edit employee data"
    data = api.SelectEmployee().get(id_)
    form = EmployeeForm(obj=data)
    if form.validate():
        api.SelectEmployee().put(id_)
        return redirect(url_for("view_bp.employees"))
    return render_template("form.html", form=form, action=action, title=title)


@view_bp.route("/delete_employee/<int:id_>", methods=["POST"])
def delete_employee(id_):
    """
    A delete command for employee.
    """
    api.SelectEmployee().delete(id_)
    return redirect(url_for("view_bp.employees"))
