"""This module declares input forms for departments and employees.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, SelectField, DateField
from wtforms.validators import DataRequired, Length, NumberRange

from department_app.service import api


class DepartmentForm(FlaskForm):

    """
    Department form with corresponding data fields and validators.
    """

    name = StringField(
        "Name",
        [
            DataRequired(),
            Length(max=49, message=("Name is too long")),
        ],
    )

    submit = SubmitField("Submit")


class EmployeeForm(FlaskForm):

    """
    Employee form with corresponding data fields and validators.
    """

    _date_format = "[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}"
    _choices = []

    def __init__(self, *args, **kwargs):
        # Gets values for drop-down list every time instance of
        # a class is instantiated (evepy page load), so the list is updated
        # with new departments.
        # I know this is ugly and it is better to cache depts upon change
        # in API and to access the cache from here. But it works for now.
        # TODO Refactor
        self._choices.clear()
        all_depts = api.ListDepartments().get()
        depts = [(dept.id, dept.name) for dept in all_depts]
        depts.append(("no_dept", "<No department>"))
        depts.sort(key=lambda x: x[1].casefold())
        self._choices.extend(depts)
        super().__init__(*args, **kwargs)

    first_name = StringField(
        "First name",
        [
            DataRequired(),
            Length(max=29, message=("Name is too long")),
        ],
    )

    second_name = StringField(
        "Second name",
        [
            DataRequired(),
            Length(max=29, message=("Name is too long")),
        ],
    )

    birth_date = DateField(
        "Date of birth (YYYY-MM-DD)",
        [DataRequired()],
    )
    salary = IntegerField(
        "Salary amount",
        [
            DataRequired(),
            NumberRange(min=1, message=("Zero values and less are not accepted")),
        ],
    )

    dept_id = SelectField("Choose department", choices=_choices)

    submit = SubmitField("Submit")
