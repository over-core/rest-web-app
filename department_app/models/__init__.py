"""
This module creates instances for DB management components. Components get
get imported to other modules.
"""
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()
