"""
Module defines table data, relations and restrictions.

    * Department - has one-to-many relationship with employees
    * Employees - has 'dept' backref to access department assigned to employee
"""
# pylint: disable=R0903

from datetime import datetime as dt
from department_app.models import db


class Department(db.Model):
    """
    Department model. Has one-to-many relationship with employee model.
    """

    __tablename__ = "departments"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    created = db.Column(db.DateTime, unique=False, nullable=False, default=dt.now())
    employees = db.relationship("Employee", backref="dept", lazy=True)


class Employee(db.Model):
    """
    Employee model. Foreign key is not nullable, thus deleting department with
    employees is impossible.
    """

    __tablename__ = "employees"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30), unique=False, nullable=False)
    second_name = db.Column(db.String(30), unique=False, nullable=False)
    birth_date = db.Column(db.Date, unique=False, nullable=False)
    salary = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, unique=False, nullable=False, default=dt.now())
    dept_id = db.Column(db.Integer, db.ForeignKey("departments.id"), nullable=False)
