"""
This is driver code to run API web service, which will be accessible from cli.
"""
from department_app import create_api

prod = create_api("config.ProdApp")

if __name__ == "__main__":
    dev = create_api("config.DevApp")
    dev.run()
