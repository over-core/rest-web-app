"""This is driver code to run web application.
"""

from department_app import create_app

prod = create_app("config.ProdApp")

if __name__ == "__main__":
    dev = create_app("config.DevApp")
    dev.run()
