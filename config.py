"""This module does initial Flask configuration by passing all constants to
the application context. It gets called from __init__ in department_app folder
by from_object() function.
Config can be extended by supplying .env file with values. It will load .env
contents with load_dotenv() and they should be passed in classes with
os.environ.get("DOTENV_VARIABLE")
It has base Config class, which is extended by further classes for development
and production environments.
"""
# pylint: disable=R0903

from os import environ, urandom

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())


class Config:

    """Base config"""

    SECRET_KEY = environ.get("SECRET_KEY") or urandom(16)
    # [DB_TYPE]+[DB_CONNECTOR]://[USERNAME]:[PASSWORD]@[HOST]:[PORT]/[DB_NAME][?key=value..]
    SQLALCHEMY_DATABASE_URI = environ.get("POSTGRES_DATABASE_URI")
    STATIC_FOLDER = "static"
    TEMPLATES_FOLDER = "templates"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # get flask_restful parser errors
    BUNDLE_ERRORS = True


class DevApp(Config):

    """Development config for webapp.py"""

    FLASK_APP = environ.get("FLASK_APP")
    FLASK_ENV = environ.get("FLASK_ENV_DEV")
    FLASK_DEBUG = True
    DEBUG = True
    TESTING = True
    SERVER_NAME = "localhost.localdomain:9000"
    SQLALCHEMY_ECHO = True
    BOOTSTRAP_SERVE_LOCAL = True


class DevApi(Config):

    """Development config for webservice.py"""

    FLASK_APP = environ.get("FLASK_API")
    FLASK_ENV = environ.get("FLASK_ENV_DEV")
    FLASK_DEBUG = True
    DEBUG = True
    TESTING = True
    SERVER_NAME = "localhost.localdomain:9001"
    SQLALCHEMY_ECHO = True


class PytestApp(Config):

    """Handling CSRF for tests"""

    WTF_CSRF_ENABLED = False


class ProdApp(Config):

    """Production config for webapp.py"""

    FLASK_APP = environ.get("FLASK_APP")
    FLASK_ENV = environ.get("FLASK_ENV_PROD")
    # SERVER_NAME = "localhost.localdomain:9000"
    SQLALCHEMY_ECHO = False
    BOOTSTRAP_SERVE_LOCAL = True


class ProdApi(Config):

    """Production config for webservice.py"""

    FLASK_APP = environ.get("FLASK_API")
    FLASK_ENV = environ.get("FLASK_ENV_PROD")
    # SERVER_NAME = "localhost.localdomain:9001"
    SQLALCHEMY_ECHO = False
