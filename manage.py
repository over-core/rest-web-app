"""This module has attached Flask-Migrate components, so it should be initially
used to create tables with commands in shell:
'export FLASK_APP=manage.py'
'flask db init' - create migration files in department_app/migrations
'flask db migrate' - find difference in connected database and defined models
'flask db upgrade' - perform migration
"""
from department_app import migration

app = migration("config.Config")

if __name__ == "__main__":
    app.run()
