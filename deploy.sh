#!/bin/bash
if [ -d ".venv" ]
then
    source .venv/bin/activate
    pip install .
    export FLASK_APP=manage.py
    flask db init
    flask db migrate
    flask db upgrade
else
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install --upgrade pip
    pip install .
    export FLASK_APP=manage.py
    flask db init
    flask db migrate
    flask db upgrade
fi