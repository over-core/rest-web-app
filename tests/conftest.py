"""
This module configures provides pytest fixtures used in test to simulate
deployed environment.
"""

import pytest

from department_app import create_app, create_api
from department_app.models import db
from department_app.models.tables import Department, Employee


@pytest.fixture(scope="module")
def test_app_client():
    """
    Creates instance of browser web app for testing.
    """

    app = create_app("config.PytestApp")
    with app.test_client() as test_client:
        with app.app_context():
            yield test_client  # testing


@pytest.fixture(scope="module")
def test_api_client():
    """
    Creates instance of API web service for testing.
    """

    api = create_api("config.DevApi")
    with api.test_client() as test_client:
        with api.app_context():
            yield test_client  # testing


@pytest.fixture(scope="module")
def init_database():
    """
    Initialize data in DB which will be tested.
    """
    db.drop_all()
    db.create_all()

    dept1 = Department(name="Writer dept")
    dept2 = Department(name="Empty dept")

    employee1 = Employee(
        first_name="Philip",
        second_name="Dick",
        birth_date="1928-12-16",
        salary="2000",
        dept_id="1",
    )
    employee2 = Employee(
        first_name="Kurt",
        second_name="Vonnegut",
        birth_date="1922-11-11",
        salary="3000",
        dept_id="1",
    )
    employee3 = Employee(
        first_name="William",
        second_name="Gibson",
        birth_date="1948-3-17",
        salary="3000",
        dept_id="1",
    )

    db.session.add(dept1)
    db.session.add(dept2)
    db.session.add(employee1)
    db.session.add(employee2)
    db.session.add(employee3)

    db.session.commit()

    yield  # testing

    # removing ALL (!) tables
    db.session.remove()
    db.drop_all()
    db.create_all()
