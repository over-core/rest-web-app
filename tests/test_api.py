"""
This module contains tests for web service api to be run against postgesql db.
"""
# pylint: disable=unused-argument
def test_get_departments(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments' endpoint is accessed with GET
    THEN check the data from db
    """
    response = test_api_client.get("/api/departments")
    assert response.status_code == 200
    assert b"Writer dept" in response.data
    assert b"Empty dept" in response.data


def test_post_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments' endpoint is accessed with POST and data provided
    THEN confirm data addition to db
    """
    data = {"name": "TEST dept"}
    response = test_api_client.post("/api/departments", json=data)
    assert response.status_code == 201
    assert b"TEST dept" in response.data


def test_post_duplicate_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments' endpoint is accessed with POST and duplicate data provided
    THEN confirm bad request and server response
    """
    data = {"name": "TEST dept"}
    response = test_api_client.post("/api/departments", json=data)
    assert response.status_code == 400
    assert b"such row already exists" in response.data


def test_post_no_data_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments' endpoint is accessed with POST and no data provided
    THEN confirm bad request and user receive hint for valid data
    """
    data = {}
    response = test_api_client.post("/api/departments", json=data)
    assert response.status_code == 400
    assert b"name" in response.data


def test_post_invalid_data_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments' endpoint is accessed with POST and invalid data provided
    THEN confirm bad request and server response
    """
    data = {"name": ""}
    response = test_api_client.post("/api/departments", json=data)
    assert response.status_code == 400
    assert b"non-empty string required" in response.data


def test_get_employees(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with GET
    THEN check the data from db
    """
    response = test_api_client.get("/api/employees")
    assert response.status_code == 200
    assert b"Philip" in response.data
    assert b"Kurt" in response.data
    assert b"William" in response.data


def test_post_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and data provided
    THEN confirm data addition to db
    """
    data = {
        "first_name": "Isaac",
        "second_name": "Asimov",
        "birth_date": "1920-01-02",
        "salary": 3000,
        "dept_id": 1,
    }
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 201
    assert b"Isaac" in response.data
    assert b"3000" in response.data


def test_post_duplicate_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and duplicate data provided
    THEN confirm bad request and server response
    """
    data = {
        "first_name": "Isaac",
        "second_name": "Asimov",
        "birth_date": "1920-01-02",
        "salary": 3000,
        "dept_id": 1,
    }
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 400
    assert b"such row already exists" in response.data


def test_post_no_data_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and no data provided
    THEN confirm bad request and user receive hint for valid data
    """
    data = {}
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 400
    assert b"first_name" in response.data
    assert b"second_name" in response.data
    assert b"birth_date" in response.data
    assert b"salary" in response.data
    assert b"dept_id" in response.data


def test_post_invalid_name_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and invalid name provided
    THEN confirm bad request and server response
    """
    data = {"first_name": ""}
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 400
    assert b"non-empty string required" in response.data
    assert b"second_name" in response.data
    assert b"birth_date" in response.data
    assert b"salary" in response.data
    assert b"dept_id" in response.data


def test_post_invalid_birthday_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and invalid birth date provided
    THEN confirm bad request and server response
    """
    data = {
        "first_name": "Robert",
        "second_name": "Heinlein",
        "birth_date": "07-07-1907",
        "salary": 3000,
        "dept_id": 1,
    }
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 400
    assert b"date is in wrong format or out of range" in response.data


def test_post_invalid_department_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and invalid department provided
    THEN confirm bad request and server response
    """
    data = {
        "first_name": "Robert",
        "second_name": "Heinlein",
        "birth_date": "1907-07-07",
        "salary": 3000,
        "dept_id": 10,
    }
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 404
    assert b"department not found - id" in response.data


def test_post_invalid_salary_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees' endpoint is accessed with POST and invalid salary provided
    THEN confirm bad request and server response
    """
    data = {
        "first_name": "Robert",
        "second_name": "Heinlein",
        "birth_date": "1907-07-07",
        "salary": 0,
        "dept_id": 1,
    }
    response = test_api_client.post("/api/employees", json=data)
    assert response.status_code == 400
    assert b"salary should be higher than 0" in response.data


def test_get_index_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/1' endpoint is accessed with GET
    THEN confirm status code and received data
    """
    response = test_api_client.get("/api/departments/1")
    assert response.status_code == 200
    assert b"Writer" in response.data


def test_put_index_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/1' endpoint is accessed with PUT and data sent
    THEN confirm status code and data
    """
    data = {"name": "Sci-Fi Writer dept"}
    response = test_api_client.put("/api/departments/1", json=data)
    check = test_api_client.get("/api/departments/1")
    assert response.status_code == 204
    assert b"Sci-Fi Writer dept" in check.data


def test_delete_index_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/2' endpoint is accessed with DELETE and data removed
    THEN confirm status code and data
    """
    response = test_api_client.delete("/api/departments/2")
    check = test_api_client.get("/api/departments/2")
    assert response.status_code == 204
    assert check.status_code == 404


def test_wrong_index_department(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/2' endpoint is accessed with GET, PUT, DELETE
    THEN confirm 404 status code
    """
    data = {"name": "Put this"}
    get_data = test_api_client.get("/api/departments/2")
    put_data = test_api_client.put("/api/departments/2", json=data)
    delete_data = test_api_client.delete("/api/departments/2")
    assert get_data.status_code == 404
    assert put_data.status_code == 404
    assert delete_data.status_code == 404


def test_delete_department_with_employees(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/1' endpoint is accessed with DELETE
    THEN confirm status code and message
    """
    response = test_api_client.delete("/api/departments/1")
    assert response.status_code == 400
    assert b"unable to delete department with employees" in response.data


def test_get_index_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees/1' endpoint is accessed with GET
    THEN confirm status code and received data
    """
    response = test_api_client.get("/api/employees/1")
    assert response.status_code == 200
    assert b"Philip" in response.data
    assert b"2000" in response.data


def test_put_index_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/employees/2' endpoint is accessed with PUT and data sent
    THEN confirm status code and data
    """
    data = {"salary": 8000}
    response = test_api_client.put("/api/employees/2", json=data)
    check = test_api_client.get("/api/employees/2")
    assert response.status_code == 204
    assert b"8000" in check.data


def test_delete_index_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/3' endpoint is accessed with DELETE and data removed
    THEN confirm status code and data
    """
    response = test_api_client.delete("/api/employees/3")
    check_data = test_api_client.get("/api/employees/3")
    assert response.status_code == 204
    assert check_data.status_code == 404


def test_wrong_index_employee(test_api_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/departments/3' endpoint is accessed with GET, PUT, DELETE
    THEN confirm 404 status code
    """
    data = {"salary": "9000"}
    get_data = test_api_client.get("/api/employees/3")
    put_data = test_api_client.put("/api/employees/3", json=data)
    delete_data = test_api_client.delete("/api/employees/3")
    assert get_data.status_code == 404
    assert put_data.status_code == 404
    assert delete_data.status_code == 404
