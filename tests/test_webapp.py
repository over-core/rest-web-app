"""
This module contains tests for web browser app to be run against postgesql db.
"""
# pylint: disable=unused-argument
def test_get_departments(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' endpoint is accessed with GET
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.get("/", headers=headers)
    assert response.status_code == 200
    assert b"<td>Writer dept</td>" in response.data
    assert b"<td>3</td>" in response.data
    assert b"<td>2666.67</td>" in response.data


def test_get_add_department_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/add_dept/' endpoint is accessed with GET
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.get("/add_dept/", headers=headers)
    assert response.status_code == 200
    assert b"New department" in response.data
    assert b"Name" in response.data


def test_post_add_department_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/add_dept/' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    data = {"name": "PYTEST"}
    response = test_app_client.post("/add_dept/", data=data, follow_redirects=True)
    check = test_app_client.get("/", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>PYTEST</td>" in check.data


def test_post_edit_department_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/edit_dept/3' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    data = {"name": "PYTEST-EDIT"}
    response = test_app_client.post("/edit_dept/3", data=data, follow_redirects=True)
    check = test_app_client.get("/", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>PYTEST-EDIT</td>" in check.data


def test_post_delete_department_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/edit_dept/3' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.post("/delete_dept/3", follow_redirects=True)
    check = test_app_client.get("/", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>PYTEST-EDIT</td>" not in check.data


def test_get_employees(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/employees' endpoint is accessed with GET
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.get("/employees", headers=headers, follow_redirects=True)
    assert response.status_code == 200
    assert b"<td>Philip</td>" in response.data
    assert b"<td>William</td>" in response.data
    assert b"<td>Kurt</td>" in response.data


def test_filter_employees(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/employees' endpoint is accessed with GET and filter data provided
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    data = {
        "dept_id": "1",
        "birthday": "1928-12-16",
    }
    response = test_app_client.post(
        "/employees", data=data, headers=headers, follow_redirects=True
    )
    assert response.status_code == 200
    assert b"<td>Philip</td>" in response.data
    assert b"<td>Dick</td>" in response.data
    assert b"<td>1928-12-16</td>" in response.data
    assert b"<td>Writer dept</td>" in response.data
    assert b"<td>Kurt</td>" not in response.data
    assert b"<td>William</td>" not in response.data


def test_filter_range_employees(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/employees' endpoint is accessed with GET and filter data provided
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    data = {
        "dept_id": "no_dept",
        "birthday": "1928-12-16/1922-11-11",  # the latter is earlier, will be reversed
    }
    response = test_app_client.post(
        "/employees", data=data, headers=headers, follow_redirects=True
    )
    assert response.status_code == 200
    assert b"<td>Philip</td>" in response.data
    assert b"<td>Dick</td>" in response.data
    assert b"<td>1928-12-16</td>" in response.data
    assert b"<td>Kurt</td>" in response.data
    assert b"<td>Vonnegut</td>" in response.data
    assert b"<td>1922-11-11</td>" in response.data
    assert b"<td>William</td>" not in response.data


def test_get_add_employee_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/add_employee/' endpoint is accessed with GET
    THEN check received data
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.get("/add_employee/", headers=headers)
    assert response.status_code == 200
    assert b"New employee" in response.data
    assert b"First name" in response.data
    assert b"Second name" in response.data
    assert b"Date of birth (YYYY-MM-DD)" in response.data
    assert b"Salary amount" in response.data
    assert b"Choose department" in response.data


def test_post_add_employee_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/add_employee/' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    data = {
        "first_name": "TEST",
        "second_name": "SUBJECT",
        "birth_date": "2000-10-10",
        "salary": "1000",
        "dept_id": "1",
    }
    response = test_app_client.post(
        "/add_employee/", data=data, headers=headers, follow_redirects=True
    )
    check = test_app_client.get("/employees", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>TEST</td>" in check.data
    assert b"<td>SUBJECT</td>" in check.data
    assert b"<td>2000-10-10</td>" in check.data
    assert b"<td>1000</td>" in check.data


def test_post_edit_employee_form(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/edit_employee/4' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    data = {
        "first_name": "TEST-EDIT",
        "second_name": "SUBJECT-EDIT",
        "birth_date": "2000-05-05",
        "salary": "5000",
        "dept_id": "2",
    }
    response = test_app_client.post(
        "/edit_employee/4", data=data, headers=headers, follow_redirects=True
    )
    check = test_app_client.get("/employees", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>TEST-EDIT</td>" in check.data
    assert b"<td>SUBJECT-EDIT</td>" in check.data
    assert b"<td>2000-05-05</td>" in check.data
    assert b"<td>5000</td>" in check.data
    assert b"<td>Empty dept</td>" in check.data


def test_post_delete_employee(test_app_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/delete_employee/4' endpoint is accessed with POST
    THEN then '/' data checked with GET
    """
    headers = {"Accept": "text/html"}
    response = test_app_client.post(
        "/delete_employee/4", headers=headers, follow_redirects=True
    )
    check = test_app_client.get("/employees", headers=headers)
    assert response.status_code == 200
    assert check.status_code == 200
    assert b"<td>TEST-EDIT</td>" not in check.data
    assert b"<td>SUBJECT-EDIT</td>" not in check.data
    assert b"<td>2000-05-05</td>" not in check.data
    assert b"<td>5000</td>" not in check.data
