# rest-web-app [![Coverage Status](https://coveralls.io/repos/gitlab/over-core/rest-web-app/badge.svg?branch=master)](https://coveralls.io/gitlab/over-core/rest-web-app?branch=master)

Graduation project for Python course. The goal is to make department and employee web application, utilizing RESTful CRUD operations and various auxiliary technologies for deployment and running.

Project includes web app components to run website with access to postgresql database and ability to make changes to it.
Changes are handled by REST API components. API may be ran as a separate webservice and accessed with `curl`. 

## Installation requirements:

1. python3.7
2. python3-dev, the Python header files to install psycopg2
3. PostgreSQL server. This package is tested on version 10.15

## Installation instructions

1. Start postgresql server.
   ```
   $ sudo service postgresql stop     // Stop the service
   $ sudo service postgresql start    // Start the service
   $ sudo service postgresql restart  // Stop and restart the service
   $ sudo service postgresql reload   // Reload the configuration without stopping the service

   // OR
   $ sudo systemctl start postgresql
   $ sudo systemctl stop postgresql
   $ sudo systemctl restart postgresql
   $ sudo systemctl reload postgresql
   $ sudo systemctl status postgresql   // Show status
   ```
1. Create postgresql database to be connected with app, using `createdb -U [username] [db_name]` syntax.
   ```
   createdb -U postgres moonbase
   ```

3. `git clone https://gitlab.com/over-core/rest-web-app.git`

4. Edit `.env.example`, and set the name of database in `POSTGRES_DATABASE_URI` constant. Change username and password if needed.
 
5. Run `deploy.sh` to download dependencies and setup `.venv` environment inside project folder. Then it will call manage.py to initialize database migrations and run them into db you defined in previous paragraph.  
Or do it manually, commands are listed from project folder perspective:
   ```
   python3 -m venv .venv
   source .venv/bin/activate
   python3 -m pip install --upgrade pip
   pip install .
   export FLASK_APP=manage.py
   flask db init
   flask db migrate
   flask db upgrade
   ```

6. Finally, run web app and API web service with gunicorn, here is localhost is used as example:
   ```
   gunicorn -w 2 -b 127.0.0.1:9000 webapp:prod
   gunicorn -w 2 -b 127.0.0.1:9001 webservice:prod
   ```

7. APP is ready to use at http://127.0.0.1:9000/  
   API available at endpoints:  
   - [GET, POST] http://127.0.0.1:9001/api/departments
   - [GET, POST] http://127.0.0.1:9001/api/employees
   - [GET, PUT, DELETE] http://127.0.0.1:9001/api/departments/[int:id]
   - [GET, PUT, DELETE] http://127.0.0.1:9001/api/employees/[int:id]