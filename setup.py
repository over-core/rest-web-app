"""This module contains basic setup for environment, including
dependencies.
"""
from setuptools import setup

setup(
    name="department-app",
    version="0.1",
    description="Department and employee registration system",
    author="Oleksii Vertehel",
    author_email="a.vertegel@pm.me",
    license="MIT",
    packages=["department_app"],
    url="https://gitlab.com/over-core/rest-web-app",
    python_requires="~=3.7",
    install_requires=[
        "Flask>=1.1.2",
        "Flask-Bootstrap>=3.3.7.1",
        "Flask-Migrate>=2.5.3",
        "Flask-RESTful>=0.3.8",
        "Flask-SQLAlchemy>=2.4.4",
        "Flask-WTF>=0.14.3",
        "psycopg2>=2.8.6",
        "python-dotenv>=0.15.0",
        "gunicorn>=20.0.4",
    ],
    zip_safe=False,
)
